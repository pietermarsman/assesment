import pandas as pd

if __name__ == "__main__":
	fn = './data/raw/woz_prices_2015_amsterdam.csv'
	woz = pd.read_csv(fn, header = 2)

	woz["average woz value"] = pd.to_numeric(woz["average woz value"], errors = "coerce")
	woz["woz-value per m2"] = pd.to_numeric(woz["woz-value per m2"], errors = "coerce")

	output_fn = "./data/processed/woz_prices.csv"
	woz.to_csv(output_fn)

	print('Done!')